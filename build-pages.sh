#!/bin/sh
# Move builds from vue and react folders and copy index.html to pages

set -xe

rm -rf public
mkdir public
mv react/build public/react
mv vue/dist public/vue
cp index.html public/index.html
