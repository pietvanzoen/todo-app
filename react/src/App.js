import { useState, useEffect } from "react";
import TodoItem from "./TodoItem";
import * as storage from './storage';
import "./App.css";

function App() {
  const [todos, setTodos] = useState(storage.fetch());

  useEffect(() => {
    storage.save(todos);
  }, [todos]);

  function newTodo() {
    setTodos(
      todos.concat({
        done: false,
        title: "",
      })
    );
  }

  function deleteTodo(index) {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
  }

  function updateTodo(todoIndex, data) {
    const newTodos = [...todos];
    newTodos[todoIndex] = {
      ...newTodos[todoIndex],
      ...data,
    };
    setTodos(newTodos);
  }

  return (
    <div className="app">
      <header className="header">
        <h1>React Todo App</h1>
        <a href="https://gitlab.com/pietvanzoen/todo-app/-/tree/main/react">source</a>
      </header>
      <form className="todo-list">
        <ul>
          {todos.map((todo, i) => (
            <TodoItem
              key={i}
              index={i}
              {...todo}
              updateTodo={updateTodo}
              deleteTodo={deleteTodo}
            />
          ))}
        </ul>
        <button type="button" onClick={newTodo}>
          + New todo
        </button>
        <pre>{ JSON.stringify(todos, null, 2) }</pre>
      </form>
    </div>
  );
}

export default App;
