const STORAGE_KEY = 'todo-app-vue';

export function fetch() {
  return JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]');
}

export function save(todos) {
  localStorage.setItem(STORAGE_KEY, JSON.stringify(todos));
}
